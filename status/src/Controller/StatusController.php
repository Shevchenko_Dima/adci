<?php
/**
 * @file
 * Contains \Drupal\status\Controller\StatusController.
 *
 */

/**

 */
namespace Drupal\status\Controller;

/**
 */
use Drupal\Core\Controller\ControllerBase;

/**
 */
class StatusController extends ControllerBase {

    /**
     * {@inheritdoc}
     *
     */
    public function statusShow() {
        $output = array();

        drupal_set_message(Hello);

        $output['#attached']['drupalSettings']['width'] = $this->config('status.settings')->get('width');
        $output['#attached']['drupalSettings']['height'] = $this->config('status.settings')->get('height');
        $output['#attached']['drupalSettings']['title'] = $this->config('status.settings')->get('status_title');

        return $output;
    }

}