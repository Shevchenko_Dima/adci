<?php
namespace Drupal\status\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
* Configure example settings for this site.
*/
class StatusConfigurationForm extends ConfigFormBase {
/**
* {@inheritdoc}
*/
public function getFormId() {
return 'status_configuration_form';
}

/**
* {@inheritdoc}
*/
protected function getEditableConfigNames() {
return ['status.settings',];
}

/**
* {@inheritdoc}
*/
public function buildForm(array $form, FormStateInterface $form_state) {
$config = $this->config('example.settings');

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['title'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Status title'),
        '#default_value' => $config->get('status_title'),
    );

    $form['status'] = array(
        '#type' => 'textarea',
        '#title' => $this->t('Status'),
        '#default_value' => $config->get('status_message'),
    );

    $form['new_width'] = array(
    '#type' => 'number',
    '#title' => $this->t('New width'),
    '#default_value' => $config->get('width'),
    );

    $form['new_height'] = array(
    '#type' => 'number',
    '#title' => $this->t('New height'),
    '#default_value' => $config->get('height'),
    );

    $special = array(
        'status'  => t('Status'),
        'warning' => t('Warning'),
        'error'   => t('Error'),
    );

    $form['values']= array(
        '#type' => 'radios',
        '#description' => t('Select options(color).'),
        '#options'  => $special,
        '#required' => false,
    );

return parent::buildForm($form, $form_state);
}
    public function validateForm(array &$form, FormStateInterface $form_state){
        if ((int)$form_state->getValue('new_width') < 0) {
            $form_state->setErrorByName('new_width', $this->t('Wrong width!'));
        } elseif ((int)$form_state->getValue('new_width') > 1000) {
            $form_state->setErrorByName('new_width', $this->t('Wrong width!'));
        }

        if ((int)$form_state->getValue('new_height') < 0) {
            $form_state->setErrorByName('new_height', $this->t('Wrong height!'));
        } elseif ((int)$form_state->getValue('new_height') > 1000) {
            $form_state->setErrorByName('new_height', $this->t('Wrong height!'));
        }
    }


/**
* {@inheritdoc}
*/
public function submitForm(array &$form, FormStateInterface $form_state){
    // Retrieve the configuration

    $this->config('status.settings')
        // Set the submitted configuration setting
        ->set('width', $form_state->getValue('new_width'))
        // You can set multiple configurations at once by making
        // multiple calls to set()
        ->set('height', $form_state->getValue('new_height'))
        ->set('status_message', $form_state->getValue('status'))
        ->set('status_title', $form_state->getValue('title'))
        ->set('option', $form_state->getValue('values'))
        ->save();

    drupal_set_message(t('Settings is safe width:@width height:@height title:@title status:@status option:@special', array(
        '@width'   => $this->config('status.settings')->get('width'),
        '@height'  => $this->config('status.settings')->get('height'),
        '@status'  => $this->config('status.settings')->get('status_message'),
        '@title'   => $this->config('status.settings')->get('status_title'),
        '@special' => $this->config('status.settings')->get('option'))));
    }
}
